.. test documentation master file, created by
   sphinx-quickstart on Mon Nov 26 10:55:46 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to test's documentation of the Omegalpes library's examples
====================================================================

.. toctree::
   :maxdepth: 2







Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
