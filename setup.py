#! usr/bin/env python3
#  -*- coding: utf-8 -*-

from setuptools import setup, find_packages
#import omegalpes_lib

setup(name='test', version='1.0', packages=find_packages())


#setup(

#    name='omegalpes_lib',
#    version=omegalpes_lib.__version__,
#    packages=find_packages(),
#    author="M. BRUGERON, B. DELINCHANT, S. HODENCQ, Y. MARECHAL, L. MORRIET, C. PAJOT, F. WURTZ",
    # TODO: add the email adress
    # author_email='',
#    description="OMEG'ALPES a linear energy systems modelling library",
#    long_description=open('README.md').read(),
#    install_requires=[
#        "PuLP == 1.6.8",
#        "Matplotlib == 2.2.2",
#        "Seaborn == 0.8.1",
#        "Numpy == 1.14.2",
#        "Pandas == 0.22.0",
#        "Python_dateutils == 2.7.3"
#    ],
#    include_package_data=True,

    # TODO: add the url
    # url='',

    # TODO: add meta data
    # classifiers=[
    #     "Programming Language :: Python",
    #     "Development Status :: ",
    #     "License :: ",
    #     "Programming Language :: Python :: 3.6",
    #     "Topic :: Energy modelling",
    # ],

#)
